extends KinematicBody2D

onready var last_position = position

const PLAYER_SPEED = 100

var current_animation=""
var beam_duration=10
var cooldown=0.1
var can_shoot=true
var hit=null
var facing_right=true

func _ready() -> void:
	pass
	#$Laser.remove_point(1)

func _physics_process(delta: float) -> void:
	$Laser.remove_point(1)
	MovePlayer(delta)
	GunCheck()
	
func GunCheck():
	#ensure length is clipped to the nearest collision
	var gunsize
	var hitobject
	var hitpos
	var tilepos
	var gunstart
	$GunRaycast.force_raycast_update()
	if facing_right:
		gunstart=$GunStartRight
		$GunRaycast.cast_to=$GunStartRight.position+Vector2(1024,0)
	else:
		gunstart=$GunStartLeft
		$GunRaycast.cast_to=$GunStartLeft.position-Vector2(1024,0)
	$Laser.position=gunstart.position
		
	$Laser.clear_points()
	if $GunRaycast.is_colliding():
		var newpos
		hitpos=$GunRaycast.get_collision_point()
		hitobject=$GunRaycast.get_collider()
		$Laser.add_point(Vector2(0,0))
		var endp:Vector2=gunstart.global_position-hitpos
		#newpos=transform.xform_inv(hitpos)
		newpos=Vector2(endp.length(),0)
		get_node("../Label").text="%s,%s: %s,%s: %s,%s: %s,%s" % 	[
			int(gunstart.global_position.x), int(gunstart.global_position.y),
			int($Laser.global_position.x), int($Laser.global_position.y),
			int(hitpos.x), int(hitpos.y),
			int(newpos.x),(newpos.y)]
		$Laser.add_point(newpos)
	if hitobject is TileMap:
		#tilepos=get_parent().get_node("Level01").world_to_map(hitpos)
		#print(hitpos-position)
		#print(tilepos)
		#print(hitpos)
		pass
	else:
		pass
		#gunsize=$GunRaycast.get_collision_point()-gunstart.position
		#print(hitobject.name)
		#print(gunsize.length())
	
func MovePlayer(delta: float):
	var x = 0
	var y = 0
	
	x = -int(Input.is_action_pressed("ui_left"))
	x += int(Input.is_action_pressed("ui_right"))
	y = int(Input.is_action_pressed("ui_down"))
	y -= int(Input.is_action_pressed("ui_up"))
	
	
	var dir=Vector2(x,y).normalized()
	if x!=0 && y!=0:
		move_and_slide(dir*PLAYER_SPEED)
	else:
		move_and_collide(dir*delta*PLAYER_SPEED)
		
	if dir.x<0:
		facing_right=false
		$AnimatedSprite.flip_h=true
		$GunRaycast.cast_to=Vector2(position-Vector2(1024,$GunStartLeft.position.y))
	if dir.x>0:
		facing_right=true
		$AnimatedSprite.flip_h=false
		$GunRaycast.cast_to=Vector2(position+Vector2(1024,$GunStartRight.position.y))
		
	if dir==Vector2.ZERO:
		if current_animation!="idle":
			$AnimatedSprite.play("idle")
	elif current_animation!="walk":
		$AnimatedSprite.play("walk")
		
