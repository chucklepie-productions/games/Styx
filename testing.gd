extends Node2D

onready var rigman:RigidBody2D =$RigidMan
onready var kinman:KinematicBody2D=$RigidMan2
var impulse=Vector2(-200,-80)
var impulse2=Vector2(-200,-80)
var speed=1200
var jumpspeed=-350
var gravity=1000
var velocity=Vector2.ZERO

func _ready() -> void:
	pass

func _physics_process(delta: float) -> void:
	if velocity.x!=0:
		if kinman.is_on_floor():
			velocity.x=lerp(velocity.x,0,0.03)
		else:
			velocity.x=lerp(velocity.x,-550,0.25)
		
	velocity.y+=gravity*delta
	velocity=kinman.move_and_slide(velocity,Vector2.UP)
	if Input.is_action_just_pressed("ui_left"):
		if kinman.is_on_floor():
			velocity.y=jumpspeed
			velocity.x=-800
		#kinman.move_and_collide(impulse2)
	
	if Input.is_action_just_pressed("ui_up"):
		rigman.apply_impulse(impulse,impulse)

	if Input.is_action_just_pressed("ui_down"):
		rigman.apply_central_impulse(impulse)
		
	if Input.is_action_just_pressed(("ui_accept")):
		get_tree().reload_current_scene()
